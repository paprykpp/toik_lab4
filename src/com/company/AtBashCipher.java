package com.company;

public class AtBashCipher implements Cipher{

    @Override
    public String encode(String message) {
        StringBuilder resultText = new StringBuilder();
        for(char c : message.toCharArray())
        {
            if(Character.isLetter(c))
            {
                if(Character.isUpperCase(c))
                    resultText.append((char) ('A' + ('Z' - c)));

                if(Character.isLowerCase(c))
                    resultText.append((char) ('a' + ('z' - c)));
            }
            else
            {
                resultText.append(c);
            }
        }
        return resultText.toString();
    }

    @Override
    public String decode(String message) {
        return encode(message);
    }
}